var gulp = require('gulp');
var cleanCss = require('gulp-clean-css');
var size = require('gulp-size');
var config = require('../config').minifyCss;

gulp.task('minify-css', ['sass'], function () {
  return gulp.src(config.src + '/*.css')
    .pipe(cleanCss(config.options))
    .pipe(gulp.dest(config.dest))
    .pipe(size())
    .pipe(size({gzip: true}));
});
