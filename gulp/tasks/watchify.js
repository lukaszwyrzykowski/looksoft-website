/**
 * Dedicated watch task for Browserify.
 * It optimizes javascript build time by compiling only changed files.
 * @see https://github.com/substack/watchify
 */
var gulp = require('gulp');
var browserifyTask = require('./browserify');

gulp.task('watchify', function(callback) {
  // Start browserify task with devMode === true
  browserifyTask(callback, true);
});
