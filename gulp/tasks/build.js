var gulp = require('gulp');
var sequence = require('gulp-sequence');

gulp.task('build', sequence(
  'clean',
  ['copy', 'minify-css', 'uglify-js']
));
