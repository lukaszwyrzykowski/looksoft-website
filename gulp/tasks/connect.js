var gulp = require('gulp');
var connect = require('gulp-connect');

gulp.task('connect', function () {
  connect.server({
    root: '_build',
    debug: true,
    hostname: '*',
    port: 8383,
    keepalive: true,
  });
});