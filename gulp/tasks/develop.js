/**
 * Main task for rapid web development.
 *
 * To use livereload feature:
 * - configure your development server to serve "static" directory directly from project.
 *   For Tomcat, add <Context> in server.xml:
 *   <Host name="localhost" ...>
 *     ...
 *     <Context docBase="[path-to-project]/app/src/main/webapp/static" path="/static"/>
 * - install LiveReload extension in your favourite browser: http://livereload.com/extensions/
 * - run "gulp develop" task
 * - enjoy coding - and forget about reloading your browser :)
 */
var gulp = require('gulp');
var gutil = require('gulp-util');
var sequence = require('gulp-sequence');

gulp.task('develop', function(cb) {
  sequence(
    'clean',
    ['sass', 'copy'],
    'watch',
    function() {
      gutil.log(gutil.colors.green('Waiting for changes...'));
      cb();
    });
});
