var gulp  = require('gulp');
var open = require('gulp-open');

gulp.task('open', function() {
  gulp.src('')
    .pipe(open({
      uri: 'http://localhost:8383',
      app: 'google-chrome --disable-translate --disable-web-security --user-data-dir=/tmp',
    }));
});