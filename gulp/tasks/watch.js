/**
 * Watch source files and run appropriate task on every change.
 * Notify LiveReload extension when generated files have changed.
 */
var gulp = require('gulp');
var livereload = require('gulp-livereload');
var path = require('path');
var config = require('../config');

gulp.task('watch', ['watchify'], function() {
  livereload.listen();

  // styles
  gulp.watch(config.paths.styles + '/**', ['sass']);
  gulp.watch(config.paths.build + '/**/*.css', livereload.changed);

  // scripts
  gulp.watch(config.paths.build + '/**/*.js', livereload.changed);

  // static files
  config.copy.files.forEach(function(el) {
    gulp.watch(el.src, ['copy']);
  });
  gulp.watch(config.copy.files.dest + '/**', livereload.changed);

});
