var gulp = require('gulp');
var path = require('path');
var Stream = require('merge-stream');
var config = require('../config').copy;

gulp.task('copy', function () {
  var stream = new Stream();

  config.files.forEach(function (el) {
    stream.add(gulp.src(el.src)
      .pipe(gulp.dest(path.join(config.dest, el.dest))));
  }.bind(this));

  return stream;
});
