var path = require('path');

var dirs = {};
dirs.fonts = 'fonts';
dirs.images = 'images';
dirs.scripts = 'js';
dirs.styles = 'style';

var paths = {};
paths.base = './src';
paths.src = paths.base;
paths.build = './_build';

paths.fonts = path.join(paths.src, dirs.fonts);
paths.images = path.join(paths.src, dirs.images);
paths.scripts = path.join(paths.src, dirs.scripts);
paths.styles = path.join(paths.src, dirs.styles);
paths.vendor = './node_modules';

module.exports = {
  paths: paths,

  clean: [
    paths.build + '/*'
  ],

  sass: {
    src: paths.styles,
    dest: path.join(paths.build, dirs.styles),
    options: {
      includePaths: [
        paths.vendor,
        paths.vendor + '/bootstrap-sass/assets/stylesheets',
        paths.vendor + '/font-awesome/scss'
      ]
    },
    autoprefixer: {
      browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1'],
      cascade: false
    }
  },

  minifyCss: {
    src: path.join(paths.build, dirs.styles),
    dest: path.join(paths.build, dirs.styles),
    options: {
      keepSpecialComments: 0 // remove all
    }
  },

  copy: {
    files: [{
      src: paths.images + '/**',
      dest: dirs.images
    }, {
      src: paths.fonts + '/**',
      dest: dirs.fonts
    }, {
      src: paths.vendor + '/bootstrap-sass/assets/fonts/bootstrap/*',
      dest: dirs.fonts + '/bootstrap'
    }, {
      src: paths.vendor + '/font-awesome/fonts/*',
      dest: dirs.fonts + '/font-awesome'
    }, {
      src: paths.vendor + '/lightgallery/dist/fonts/*',
      dest: dirs.fonts
    }, {
      src: paths.src + '/index.html',
      dest: ''
    }],
    dest: paths.build
  },

  browserify: {
    options: {
      transform: [
        [require('babelify'), { presets: ['es2015', 'react'] }]
      ]
    },
    bundles: [{
      entries: './' + paths.scripts + '/index.js',
      dest: path.join(paths.build, dirs.scripts),
      outputName: 'index.js'
    }]
  },

  uglifyJs: {
    src: path.join(paths.build, dirs.scripts),
    dest: path.join(paths.build, dirs.scripts),
    options: {}
  }
};
