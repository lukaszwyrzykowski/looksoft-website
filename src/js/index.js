import React from 'react';
import ReactDOM from 'react-dom';
import Router from './react/router/router';

window.onload = () => {
  ReactDOM.render(
    Router,
    document.getElementById('app')
  );
};