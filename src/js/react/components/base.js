import React from 'react';
import Strings from './../../config/strings';

class BaseComponent extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.strings = new Strings();
  }
}

export default BaseComponent;