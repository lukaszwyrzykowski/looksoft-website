import React from 'react';
import BaseComponent from './base';
import Swiper from 'swiper';

class SwiperContainer extends BaseComponent {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    console.log(document.querySelector('.swiper-container'))
    var s = new Swiper('.swiper-container', {
      slidesPerView: 1,
    });
  }

  render() {
    return (
      <div className="swiper-container">
        <div className="swiper-wrapper">
          <div className="swiper-slide">1</div>
          <div className="swiper-slide">2</div>
          <div className="swiper-slide">3</div>
          <div className="swiper-slide">4</div>
          <div className="swiper-slide">5</div>
          <div className="swiper-slide">6</div>
          <div className="swiper-slide">7</div>
        </div>
      </div>
    )
  }
}

export default SwiperContainer;