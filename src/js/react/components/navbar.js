import React from 'react';
import BaseComponent from './base';

class Navbar extends BaseComponent {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className="container">
        <nav className="navbar navbar-light bg-faded">
          <a className="navbar-brand" href="#">
            <img src="images/looksoft_logo.png" />
          </a>
          <ul className="nav navbar-nav pull-right">
            <li className="nav-item active">
              <a className="nav-link" href="#">{ this.strings.text.MAIN.toUpperCase() }</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">{ this.strings.text.PORTFOLIO.toUpperCase() }</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">{ this.strings.text.WHATWEDID.toUpperCase() }</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">{ this.strings.text.ABOUTUS.toUpperCase() }</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">{ this.strings.text.WORK.toUpperCase() }</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">{ this.strings.text.CONTACT.toUpperCase() }</a>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default Navbar;