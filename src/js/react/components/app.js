import React from 'react';
import BaseComponent from './base';
import Navbar from './navbar';
import Swiper from './swiper';

class App extends BaseComponent {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <Navbar/>
        <Swiper/>
      </div>
    )
  }
}

export default App;