import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import routes from './routes';

let router = (
  <Router history={browserHistory}>
    <Route path="/" component={routes.app}>

    </Route>
  </Router>
);

export default router;