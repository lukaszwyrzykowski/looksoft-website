import EventEmitter from 'events';
import Storage from './../libs/storage';

const STRINGS = {
  'pl': {
    'MAIN': 'Główna',
    'PORTFOLIO': 'Portfolio',
    'WHATWEDID': 'Co robimy',
    'ABOUTUS': 'O nas',
    'WORK': 'Praca',
    'CONTACT': 'Kontakt',
  },
  'en': {
    'MAIN': 'Main',
    'PORTFOLIO': 'Portfolio',
    'WHATWEDID': 'What we do',
    'ABOUTUS': 'O nas',
    'WORK': 'Work',
    'CONTACT': 'Contact',
  }
};

// Values.main.title = Util.getLanguage() == 'pl' ? 'GŁÓWNA' : 'MAIN';
// Values.portfolio.title = Util.getLanguage() == 'pl' ? 'PORTFOLIO' : 'PORTFOLIO';
// Values.what_are_we_doing.title = Util.getLanguage() == 'pl' ? 'CO ROBIMY' : 'WHAT WE DO';
// Values.what_are_we_doing.name = Util.getLanguage() == 'pl' ? 'Co robimy?' : 'What we do?';
// Values.what_are_we_doing.description = Util.getLanguage() == 'pl' ? 'Na zlecenie naszych klientów tworzymy aplikacje na wszystkie dostępne w Polsce platformy mobilne i Smart TV. Zrealizowaliśmy już ponad 300 projektów, a nasze aplikacje trafiły do milionów użytkowników na całym świecie. Zajmujemy się zaawansowanymi rozwiązaniami IT z dziedziny konsumpcji mediów video i mobile commerce. Specjalizujemy się też w technologiach NFC, Bluetooth, nawigacji wewnątrz budynków i wielu innych tematach, na których opiera się rynek mobilnych rozwiązań.' :
//   'For our clients we create apps for every mobile platfrom and smart TV that is available in Poland. We have carried out over 300 projects and our apps are used by millions of users all over the world. We offer advanced IT solutions in the area of video media consumption and mobile commerce. We also specialise in NFC technologies, Bluetooth, interior navigation and many other issues which are essential on the mobile solutions market.';
// Values.about_us.title = Util.getLanguage() == 'pl' ? 'O NAS' : 'ABOUT US';
// Values.about_us.name = Util.getLanguage() == 'pl' ? 'Kim jesteśmy?' : 'Who we are?';
// Values.about_us.description = Util.getLanguage() == 'pl' ? 'Looksoft to zespół specjalistów i pasjonatów z wieloletnim doświadczeniem na rynku rozwiązań IT. Duża wiedza zdobyta w setkach zrealizowanych projektów oraz pełne zaangażowanie gwarantuje udane wdrożenie Państwa aplikacji i satysfakcję z dobrze wykonanej pracy.' : "Looksoft is a team of people with passion, specialists with many years' expertise in IT solutions. Broad knowledge gained at hundreds of successfully implemented projects and great engagement guarantee you a successful launch of your app and a satisfaction from a job well done.";
// Values.work.title = Util.getLanguage() == 'pl' ? 'PRACA' : 'WORK';
// Values.work.name = Util.getLanguage() == 'pl' ? 'Pracuj u nas!' : 'Work with us!';
// Values.work.description = Util.getLanguage() == 'pl' ? 'Nasz zespół składa się z ponad 30 specjalistów z branży mobile i webdevelopmentu. Nad każdym projektem pracuje wyspecjalizowany team programistów, projektantów i kierowników projektów. Nasi pracownicy podchodzą do swoich zadań z pasją i zaangażowaniem, przykładając uwagę do detali i kluczowych funkcjonalności każdego projektu. Staramy się, aby zdobyte w zespole wiedza i doświadczenie zostało przekazane także nowym członkom zespołu, którzy zyskają szansę rozwoju zawodowego na pełnym wyzwań rynku mobile developmentu.</br></br>Aktualnie poszukujemy osób na stanowiska' : "Looksoft is a team of people with passion, specialists with many years' expertise in IT solutions. Broad knowledge gained at hundreds of successfully implemented projects and great engagement guarantee you a successful launch of your app and a satisfaction from a job well done.</br></br>At present, we are looking for people specialised in the following areas:";
// Values.contact.title = Util.getLanguage() == 'pl' ? 'KONTAKT' : 'CONTACT';
// Values.contact.name_left = Util.getLanguage() == 'pl' ? 'Kontakt' : 'Contact';
// Values.contact.description_left = Util.getLanguage() == 'pl' ? '<p>adres:</br>LookSoft Sp. z.o.o.</br>ul. Grochowska 14E (wejście od Biskupiej)</br>04-217 Warszawa</br></br>e-mail: info@looksoft.pl</br></br>tel: +48 (22) 188 13 36</p>' : '<p>address:</br>LookSoft Sp. z.o.o.</br>Grochowska 14E (Biskupia entrance)</br>04-217 Warszawa</br></br>e-mail: info@looksoft.pl</br></br>phone: +48 (22) 188 13 36</p>';
// Values.contact.name_right = Util.getLanguage() == 'pl' ? 'Napisz do Nas' : 'Write to Us';
// Values.contact.description_right = Util.getLanguage() == 'pl' ? 'Jeśli chcesz się z nami skontaktować wypełnij poniższy formularz. Odpowiemy najszybciej jak tylko będzie to możliwe.' : 'If you want to contact with us please fill out the form below. We will answer as soon as possible.';
// Values.contact.form_name = Util.getLanguage() == 'pl' ? 'Imię i nazwisko' : 'Name';
// Values.contact.form_email = Util.getLanguage() == 'pl' ? 'Twój adres email' : 'Your e-mail address';
// Values.contact.form_message = Util.getLanguage() == 'pl' ? 'Wiadomość' : 'Message';
// Values.tables.title_idea = Util.getLanguage() == 'pl' ?	'Pomysł' : 'Idea';
// Values.tables.desc_idea = Util.getLanguage() == 'pl' ? 	'Dobry pomysł na aplikację to podstawa.  Nasz zespół może doradzić, jakie funkcje warto zaimplementować a z jakich zrezygnować z korzyścią dla funkcjonalności. Doradzimy co jest  w tej chwili na topie, określimy ramy czasowe i ryzyka projektu i przedstawimy możliwe warianty jego rozwoju.' :
//   'A good idea for an app is an essential issue. Our team will advise you on what functions are worth of implementing and which of them you should give up to obtain better functionality. We will tell you what is a best-seller at the moment, define the time framework and project risks and present possible ways of its development.';
// Values.tables.title_project = Util.getLanguage() == 'pl' ?	'Projekt' : 'Project';
// Values.tables.desc_project = Util.getLanguage() == 'pl' ?	'Nasi projektanci UX przygotują profesjonalny projekt aplikacji, która będzie prosta i funkcjonalna dla użytkowników. Wraz z klientem zastanowimy się jak zaprojektować aplikację, żeby spełniała ona stawiane przed nią cele funkcjonalne i biznesowe.' :
//   'Our UX designers will prepare a professional app design, which will be useful and functional for its users. Together with the client we will design the app so that it meets all functional expectations and business goals.';
// Values.tables.title_graph = Util.getLanguage() == 'pl' ?	'Grafika' : 'Graphics';
// Values.tables.desc_graph = Util.getLanguage() == 'pl' ?	'Projekty graficzne w Looksoft są zawsze na najwyższym poziomie i uwzględniają najnowsze trendy projektowe w dziedzinie aplikacji mobilnych. Dopasowujemy wygląd do specyfiki poszczególnych platform i jednocześnie dbamy, aby projekty były zgodne z identyfikację marki klienta.' :
//   "Graphic projects prepared by Looksoft always represent the top quality and include the latest design trends in terms of mobile applications. We adjust the apps to the specifics of particular platforms but, at the same time, we focus our efforts on making the project in line with the client's brand identification.";
// Values.tables.title_dev = Util.getLanguage() == 'pl' ?	'Development' : 'Development';
// Values.tables.desc_dev = Util.getLanguage() == 'pl' ?	'Looksoft to zespół doświadczonych programistów specjalizujących się w natywnym programowaniu na platformy mobilne iOS/Android/Windows Phone a także Smart TV oraz Web. Dbamy o jakość i przejrzystość naszego kodu i bierzemy za niego pełną odpowiedzialność. ' :
//   'Looksoft is a team of experienced programmers specialised in native programming on iOS, Android and Windows Phone mobile platforms, as well as on Smart TV and Web. We care about the quality and transparency of our code and we take full responsibility for it.s';
// Values.tables.title_test = Util.getLanguage() == 'pl' ?	'Testowanie' : 'Testing';
// Values.tables.desc_test = Util.getLanguage() == 'pl' ?	'W procesie testowania wykorzystujemy zaawansowane narzędzia analityczne a testowane aplikacje wgrywamy na wszystkie dostępne w naszym TestLabie urządzenia. Staramy się stale rozwijać naszą bazę urządzeń testowych, żeby zagwarantować minimalną liczbę błędów.' :
//   'In the testing process we use advanced analytic tools and all tested apps are uploaded on all devices available in our TestLab. We constantly develop our base of testing devices to guarantee the most reliable tests on devices that are commonly used in the market.';
// Values.tables.title_publish = Util.getLanguage() == 'pl' ?	'Publikacja' : 'Publication';
// Values.tables.desc_publish = Util.getLanguage() == 'pl' ?	'Przygotowujemy wszystkie materiały potrzebne do procesu publikacji aplikacji na marketach: opisy, filmy, grafiki i inne materiały marketingowe. W porozumieniu z klientem umieszczamy gotowe aplikacje do pobrania w marketach iTunes, Google Play, Windows Marketplace oraz Smart TV.' :
//   'We prepare all necessary materials needed in the process of the app publication in app stores: descriptions, films, graphics and other marketing materials. On agreement with the client, we place the apps which are ready to download in AppStore, Google Play, Windows Marketplace and Smart TV.';
// Values.tables.title_pr = Util.getLanguage() == 'pl' ?	'PR' : 'PR';
// Values.tables.desc_pr = Util.getLanguage() == 'pl' ?	'Staramy się wspierać naszego klienta w działaniach PR. Przygotowujemy materiały i notki prasowe i wysyłamy je do szerokiej bazy odbiorców w mediach polskich i zagranicznych.' :
//   'We do our best to support our client in their PR activity. We prepare materials and press releases, and we send them to the broad base of adressees in Polish and foreign media.';

class Strings extends EventEmitter {
  constructor(props) {
    super(props);
    this.storage = new Storage();
    this.language = this.getLanguage();

    this.text = {
      'MAIN': STRINGS[this.language].MAIN,
      'PORTFOLIO': STRINGS[this.language].PORTFOLIO,
      'WHATWEDID': STRINGS[this.language].WHATWEDID,
      'ABOUTUS': STRINGS[this.language].ABOUTUS,
      'WORK': STRINGS[this.language].WORK,
      'CONTACT': STRINGS[this.language].CONTACT,
    };
  }

  getLanguage() {
    const stored = this.storage.getItem(this.storage.namespace);

    if(stored) {
      return stored.language;
    }

    const language = navigator.language.toLowerCase();
    this.storage.setItem(this.storage.namespace, {
      'language': language,
    });

    return language;
  }

  changeLanguage(language) {
    this.storage.setItem(this.storage.namespace, {
      'language': language,
    });
  }
}

export default Strings;