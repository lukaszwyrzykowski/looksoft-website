import EventEmitter from 'events';

class Storage extends EventEmitter{
  constructor() {
    super();
    this.namespace = 'looksoft';
  }

  isAvailable() {
    return (typeof window.Storage !== 'undefined');
  }

  getItem(key) {
    if (this.isAvailable()) {
      var val = localStorage.getItem(key);
      return val ? JSON.parse(val) : undefined;
    }
  }

  setItem(key, val) {
    if (this.isAvailable()) {
      localStorage.setItem(key, JSON.stringify(val));
    }
  }

  removeItem(key) {
    if (this.isAvailable()) {
      localStorage.removeItem(key);
    }
  }
}

export default Storage;